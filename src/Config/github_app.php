<?php

return [
    /**
     * URL to GitHub, probably wont ever need to change this
     */
    'github_api_url' => 'https://api.github.com/',

    /**
     * This is the URL to your GitHub App, you can find this out by running the getAccessTokenURLForGitHubApp() method
     */
    'app_access_token_url' => 'https://api.github.com/app/installations/-some number-/access_tokens',

    /**
     * This is required by GitHub, usually use you GitHub username
     */
    'user_agent' => 'your-github-user',

    /**
     * When you create a GitHub app, you can create downloadable private keys in pem format, save it into the project
     * and put the file path here.
     */
    'pem_file_path' => storage_path() . '/keys/your-pem-file.private-key.pem'
];
