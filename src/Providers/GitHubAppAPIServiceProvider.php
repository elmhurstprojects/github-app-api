<?php

namespace ElmhurstProjects\GitHubAppAPI\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class GitHubAppAPIServiceProvider extends ServiceProvider
{
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../Config/github_app.php' => config_path('github_app.php'),
        ], 'github_app');

        parent::boot();
    }
}
