<?php

namespace ElmhurstProjects\GitHubAppAPI\Managers;

use Carbon\Carbon;
use SimpleJWT\JWT;
use SimpleJWT\Keys\KeySet;
use SimpleJWT\Keys\RSAKey;
use stdClass;

class GitHubAppAPIManager
{
    protected string $endpoint_url;
    protected string $access_token_url;
    protected string $user_agent;

    public function __construct()
    {
        $this->endpoint_url = config('github_app.github_api_url');

        $this->access_token_url = config('github_app.app_access_token_url');

        $this->user_agent = config('github_app.user_agent');
    }

    public function get(string $endpoint)
    {
        return $this->call($endpoint, 'GET');
    }

    public function post(string $endpoint, array $data)
    {
        return $this->call($endpoint, 'POST', $data);
    }

    public function put(string $endpoint, array $data)
    {
        return $this->call($endpoint, 'PUT', $data);
    }

    public function patch(string $endpoint, array $data)
    {
        return $this->call($endpoint, 'PATCH', $data);
    }

    public function delete(string $endpoint, array $data = [])
    {
        return $this->call($endpoint, 'DELETE', $data);
    }

    /**
     * Fires an API endpoint, if have data then a POST request will be sent
     *
     * @param string $endpoint
     * @param $method
     * @param stdClass|null $data
     *
     * @return mixed
     * @throws \JsonException
     */
    protected function call(string $endpoint, $method, $data = null)
    {
        $ch = curl_init($this->endpoint_url . $endpoint);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getStandardHeadersForAPI());

        return $this->executeCurl($ch, $method, $data);
    }

    /**
     * Returns an JWT token based on the PEM file set in the config, this is then used on the specified access_token_url that
     * you can get from the getAccessTokenURLForGitHubApp() method.
     * @return string
     */
    public function createJWTTokenFromPEMFile(): string
    {
        $set = new KeySet();

        $key = new RSAKey(file_get_contents(config('github_app.pem_file_path')), 'pem');

        $set->add($key);

        $headers = ['alg' => 'RS256', 'typ' => 'JWT'];

        $claims = ['iss' => 126814, 'exp' => Carbon::now()->addMinutes(5)->timestamp];

        $jwt = new JWT($headers, $claims);

        return $jwt->encode($set);
    }

    /**
     * Returns the access token url for the GitHub app, this URL is used to get the actual access token for the API, so once you have
     * the token URL, you should store that and use it, rather than calling each time. You can have multiple URLs, I have not allowed for
     * this, it just takes the first one.
     * @return string
     * @throws \JsonException
     */
    public function getAccessTokenURLForGitHubApp(): string
    {
        $ch = curl_init('https://api.github.com/integration/installations');

        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getStandardHeadersForAPI('pem'));

        $result = $this->executeCurl($ch, 'GET');

        return $result[0]->access_tokens_url;
    }

    /**
     * This creates a very short lived token that is used in the API call that you fire straight away
     * @return string
     * @throws \JsonException
     */
    public function getAccessTokenForAPI(): string
    {
        $ch = curl_init($this->access_token_url);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getStandardHeadersForAPI('pem'));

        return $this->executeCurl($ch, 'POST')->token; // Empty array makes it a POST request
    }

    /**
     * Executes the Curl command, just need to set the headers and the url in the init on the $ch passed in first
     *
     * @param $ch
     * @param $method
     * @param array|null $data
     *
     * @throws \JsonException
     */
    protected function executeCurl($ch, string $method, $data = null)
    {
        curl_setopt($ch, CURLOPT_POST, $data !== null);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        if ($data !== null) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->createJsonBody($data));
        }

        $result = curl_exec($ch);

        curl_close($ch);

        return json_decode($result, false, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * Returns the normal headers for an API call including the Bearer Auth
     *
     * @param string $token_type - 'oauth',  'pem'
     *
     * @return string[]
     * @throws \JsonException
     */
    protected function getStandardHeadersForAPI(string $token_type = 'oauth'): array
    {
        $authorization = "Authorization: Bearer ";

        $authorization .= ($token_type === 'pem') ? $this->createJWTTokenFromPEMFile() : $this->getAccessTokenForAPI();

        return ['Content-Type: application/json', $authorization, 'User-Agent: ' . $this->user_agent];
    }

    /**
     * Create a JSON encoded version of an array of parameters.
     *
     * @param array $data Request parameters
     *
     * @return string|null
     */
    protected function createJsonBody(array $data): ?string
    {
        return (count($data) === 0) ? null : json_encode($data, empty($data) ? JSON_FORCE_OBJECT : 0);
    }
}
